package ch.medtech.backend.serializer;

import ch.medtech.backend.entities.TabDocChEntity;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class TabDocVersionedDocumentOnlySerializer extends StdSerializer<TabDocChEntity> {

    public TabDocVersionedDocumentOnlySerializer() {
        this(null);
    }

    public TabDocVersionedDocumentOnlySerializer(Class<TabDocChEntity> t) {
        super(t);
    }

    @Override
    public void serialize(TabDocChEntity value, JsonGenerator gen, SerializerProvider provider)
            throws IOException {
        gen.writeObject(value.getVersionedDocument());
    }
}
