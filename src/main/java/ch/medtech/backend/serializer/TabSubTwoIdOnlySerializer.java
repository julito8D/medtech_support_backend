package ch.medtech.backend.serializer;

import ch.medtech.backend.entities.TabSubTwoChEntity;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class TabSubTwoIdOnlySerializer extends StdSerializer<TabSubTwoChEntity> {
    public TabSubTwoIdOnlySerializer() {
        this(null);
    }

    public TabSubTwoIdOnlySerializer(Class<TabSubTwoChEntity> t) {
        super(t);
    }

    @Override
    public void serialize(TabSubTwoChEntity value, JsonGenerator gen, SerializerProvider provider)
            throws IOException {
        gen.writeString(value.getId());
    }
}
