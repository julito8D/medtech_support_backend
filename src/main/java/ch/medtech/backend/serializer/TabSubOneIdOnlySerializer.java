package ch.medtech.backend.serializer;

import ch.medtech.backend.entities.TabSubOneChEntity;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class TabSubOneIdOnlySerializer extends StdSerializer<TabSubOneChEntity> {
    public TabSubOneIdOnlySerializer() {
        this(null);
    }

    public TabSubOneIdOnlySerializer(Class<TabSubOneChEntity> t) {
        super(t);
    }

    @Override
    public void serialize(TabSubOneChEntity value, JsonGenerator gen, SerializerProvider provider)
            throws IOException {
        gen.writeString(value.getId());
    }
}
