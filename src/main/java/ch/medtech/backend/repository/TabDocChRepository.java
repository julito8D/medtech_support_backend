package ch.medtech.backend.repository;

import ch.medtech.backend.composite.VersionedDocument;
import ch.medtech.backend.entities.TabDocChEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TabDocChRepository extends JpaRepository <TabDocChEntity, VersionedDocument> {
    List<TabDocChEntity> findByVersionedDocumentId(String id);

}

