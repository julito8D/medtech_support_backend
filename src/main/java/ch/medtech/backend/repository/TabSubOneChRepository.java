package ch.medtech.backend.repository;

import ch.medtech.backend.entities.TabSubOneChEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TabSubOneChRepository extends JpaRepository<TabSubOneChEntity, String> {
}
