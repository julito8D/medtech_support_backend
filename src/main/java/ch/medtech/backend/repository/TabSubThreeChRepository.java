package ch.medtech.backend.repository;

import ch.medtech.backend.entities.TabSubThreeChEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TabSubThreeChRepository extends JpaRepository <TabSubThreeChEntity, String> {
}
