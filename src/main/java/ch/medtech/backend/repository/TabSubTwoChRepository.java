package ch.medtech.backend.repository;

import ch.medtech.backend.entities.TabSubTwoChEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TabSubTwoChRepository extends JpaRepository <TabSubTwoChEntity, String> {
}
