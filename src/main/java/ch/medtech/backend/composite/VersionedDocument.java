package ch.medtech.backend.composite;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
@Embeddable
public class VersionedDocument implements Serializable {
    @Column(name = "ID")
    private String id;

    @Column(name = "VERSION_DATE")
    private LocalDate versionDate;

    public VersionedDocument() {}

    public VersionedDocument(String id, LocalDate versionDate) {
        this.id = id;
        this.versionDate = versionDate;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) {
            return true;
        }
        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }

        VersionedDocument vd = (VersionedDocument) o;
        return Objects.equals(id, vd.id) &&
                Objects.equals( versionDate, vd.versionDate );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, versionDate );
    }
}
