package ch.medtech.backend.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="id")
@Table(name = "TAB_SUB_ONE_CH")
public class TabSubOneChEntity {
    @Id
    @Column (name = "ID")
    private String id;

    @Column (name = "TITLE")
    private String title;

    @ManyToOne
    @JsonBackReference
    @JoinColumns({
            @JoinColumn (name = "TAB_DOC_CH_FK_ID"),
            @JoinColumn (name = "TAB_DOC_CH_FK_VERSION_DATE")
    })
    private TabDocChEntity tabDocChEntity;

    @OneToMany (mappedBy = "tabSubOneChEntity")
    @JsonManagedReference
    private List<TabSubTwoChEntity> tabSubTwoChEntities;

    @OneToMany (mappedBy = "tabSubOneChEntity")
    private List<TabSubThreeChEntity> tabSubThreeChEntities;
}
