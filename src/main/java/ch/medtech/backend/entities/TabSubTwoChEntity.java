package ch.medtech.backend.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="id")
@Table(name = "TAB_SUB_TWO_CH")
public class TabSubTwoChEntity {
    @Id
    @Column (name = "ID")
    private String id;

    @Column (name = "TITLE")
    private String title;

    @ManyToOne
    @JoinColumn (name = "TAB_SUB_ONE_CH_FK_ID")
    private TabSubOneChEntity tabSubOneChEntity;

    @OneToMany (mappedBy = "tabSubTwoChEntity")
    private List<TabSubThreeChEntity> tabSubThreeChEntities;
}
