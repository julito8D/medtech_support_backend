package ch.medtech.backend.entities;

import ch.medtech.backend.composite.VersionedDocument;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="versionedDocument")
@Table(name = "TAB_DOC_CH")
public class TabDocChEntity implements Serializable {
    @EmbeddedId
    private VersionedDocument versionedDocument;

    @Column (name = "TITLE_LONG")
    private String titleLong;

    @Column (name = "TITLE_SHORT")
    private String titleShort;

    @Column (name = "CONTENT")
    private String content;

    @OneToMany (mappedBy = "tabDocChEntity")
    @JsonManagedReference
    private List<TabSubOneChEntity> tabSubOneChEntities;
}
