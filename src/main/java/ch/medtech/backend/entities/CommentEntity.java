package ch.medtech.backend.entities;

import ch.medtech.backend.serializer.TabDocVersionedDocumentOnlySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table (name = "TAB_DOC_CH_COMMENT")
public class CommentEntity {
    @Id
    @Column (name = "ID")
    private String id;

    @Column (name="NAME")
    private String name;

    @Column (name = "COMMENT")
    private String comment;

    @Column (name = "CURRENTDATE")
    private LocalDateTime currentDate;

    @OneToOne
    @JoinColumns({
            @JoinColumn (name = "TAB_DOC_CH_FK_ID"),
            @JoinColumn (name = "TAB_DOC_CH_FK_VERSION_DATE"),
    })
    @JsonSerialize(using = TabDocVersionedDocumentOnlySerializer.class)
    private TabDocChEntity tabDocChEntity;
}
