package ch.medtech.backend.entities;

import ch.medtech.backend.serializer.TabDocVersionedDocumentOnlySerializer;
import ch.medtech.backend.serializer.TabSubOneIdOnlySerializer;
import ch.medtech.backend.serializer.TabSubTwoIdOnlySerializer;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="id")
@Table(name = "TAB_SUB_THREE_CH")
public class TabSubThreeChEntity {
    @Id
    @Column(name = "ID")
    private String id;

    @Column (name = "TITLE")
    private String title;

    @Column (name = "CONTENT")
    private String content;

    @OneToOne
    @JoinColumns({
            @JoinColumn (name = "TAB_DOC_CH_FK_ID"),
            @JoinColumn (name = "TAB_DOC_CH_FK_VERSION_DATE"),
    })
    @JsonSerialize(using = TabDocVersionedDocumentOnlySerializer.class)
    private TabDocChEntity tabDocChEntity;

    @ManyToOne
    @JoinColumn (name = "TAB_SUB_ONE_CH_FK_ID")
    @JsonSerialize(using = TabSubOneIdOnlySerializer.class)
    private TabSubOneChEntity tabSubOneChEntity;

    @ManyToOne
    @JoinColumn (name = "TAB_SUB_TWO_CH_FK_ID")
    @JsonSerialize(using = TabSubTwoIdOnlySerializer.class)
    private TabSubTwoChEntity tabSubTwoChEntity;
}

