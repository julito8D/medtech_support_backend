package ch.medtech.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedtechbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedtechbackendApplication.class, args);
	}

}
