package ch.medtech.backend.service;

import ch.medtech.backend.entities.TabSubThreeChEntity;
import ch.medtech.backend.repository.TabSubThreeChRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TabSubThreeChService {
    @Autowired
    private TabSubThreeChRepository tabSubThreeChRepository;

    public List<TabSubThreeChEntity> getAllArticles() {
        return tabSubThreeChRepository.findAll();
    }
}
