package ch.medtech.backend.service;

import ch.medtech.backend.composite.VersionedDocument;
import ch.medtech.backend.dto.CommentDto;
import ch.medtech.backend.entities.CommentEntity;
import ch.medtech.backend.entities.TabDocChEntity;
import ch.medtech.backend.repository.CommentRepository;
import ch.medtech.backend.repository.TabDocChRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private TabDocChRepository docRepository;

    public CommentEntity getComment(String id) {
        return commentRepository.getById(id);
    }

    public List<CommentEntity> getComments() {
        return commentRepository.findAll();
    }


    //localhost:8181/doc-id/doc-date
    public List<CommentEntity> getByDocIdAndVersionDate (String docId, LocalDate docDate) {
        Optional<TabDocChEntity> relatedDoc = docRepository.findById(new VersionedDocument(
                docId,
                docDate
        ));
        if (relatedDoc.isEmpty()){
            return new ArrayList<>();
        }
        return commentRepository.findByTabDocChEntity(relatedDoc.get());
    }
    

    public CommentEntity saveComment (CommentDto dto) {
        Optional <TabDocChEntity> relatedDoc = docRepository.findById(new VersionedDocument(
                dto.getDocId(), dto.getDocVersionDate()
        ));
        if (relatedDoc.isEmpty()){
            return null;
        }
        CommentEntity entity = dto.toEntity(relatedDoc.get());
        return commentRepository.save(entity);
    }
}