package ch.medtech.backend.service;

import ch.medtech.backend.composite.VersionedDocument;
import ch.medtech.backend.entities.TabDocChEntity;
import ch.medtech.backend.repository.TabDocChRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TabDocChService {

    @Autowired
    private TabDocChRepository tabDocChRepository;

    public List<TabDocChEntity> getAllDocuments() {
        return tabDocChRepository.findAll();
    }

    public List<TabDocChEntity> getDocuments(String id) {
        return tabDocChRepository.findByVersionedDocumentId(id);
    }

    public TabDocChEntity getDocument(String id, LocalDate date) {
        VersionedDocument vd = new VersionedDocument(id, date);
        return tabDocChRepository.findById(vd).get();
    }
}
