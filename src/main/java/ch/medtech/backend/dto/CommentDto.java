package ch.medtech.backend.dto;

import ch.medtech.backend.entities.CommentEntity;
import ch.medtech.backend.entities.TabDocChEntity;
import lombok.Data;

import javax.xml.stream.events.Comment;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class CommentDto {
    private String name;

    private String comment;

    private String docId;

    private LocalDate docVersionDate;

    public CommentEntity toEntity(TabDocChEntity docChEntity) {
        CommentEntity entity = new CommentEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setCurrentDate(LocalDateTime.now());
        entity.setName(this.name);
        entity.setComment(this.comment);
        entity.setTabDocChEntity(docChEntity);
        return entity;
    }
}
