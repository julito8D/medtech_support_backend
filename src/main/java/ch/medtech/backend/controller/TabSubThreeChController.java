package ch.medtech.backend.controller;

import ch.medtech.backend.entities.TabDocChEntity;
import ch.medtech.backend.service.TabSubThreeChService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("articles")
@CrossOrigin(origins = "http://localhost:4200")
public class TabSubThreeChController {
    @Autowired
    private TabSubThreeChService tabSubThreeChService;

    @GetMapping
    public ResponseEntity<TabDocChEntity> getAllArticles() {
        return new ResponseEntity(tabSubThreeChService.getAllArticles(), HttpStatus.OK);
    }
}
