package ch.medtech.backend.controller;

import ch.medtech.backend.entities.TabDocChEntity;
import ch.medtech.backend.service.TabDocChService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;

@Controller
@RequestMapping("documents")
@CrossOrigin(origins = "http://localhost:4200")
public class TabDocChController {

    @Autowired
    private TabDocChService tabDocChService;

    @GetMapping
    public ResponseEntity<TabDocChEntity> getAllDocuments() {
        return new ResponseEntity(tabDocChService.getAllDocuments(), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<TabDocChEntity> getAll(@PathVariable("id") String id) {
        return new ResponseEntity(tabDocChService.getDocuments(id), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}/{versionDate}")
    public ResponseEntity<TabDocChEntity> get(@PathVariable("id") String id, @PathVariable("versionDate") String versionDateString) {
        LocalDate versionDate = LocalDate.parse(versionDateString);
        return new ResponseEntity(tabDocChService.getDocument(id, versionDate), HttpStatus.OK);
    }

}