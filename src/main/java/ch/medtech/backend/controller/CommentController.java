package ch.medtech.backend.controller;

import ch.medtech.backend.dto.CommentDto;
import ch.medtech.backend.entities.CommentEntity;
import ch.medtech.backend.service.CommentService;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;

@Controller
@RequestMapping("comments")
@CrossOrigin(origins = "http://localhost:4200")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping(path = "/{id}")
    public ResponseEntity<CommentEntity> get(@PathVariable("id") String id) {
        return new ResponseEntity(commentService.getComment(id), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<CommentEntity>> getAll() {
        return new ResponseEntity<>(commentService.getComments(), HttpStatus.OK);
    }

    @GetMapping("/{docId}/{docDate}")
    public ResponseEntity<List<CommentEntity>> getByDocIdAndVersionDate(@PathVariable("docId") String docId,
                                                                        @PathVariable("docDate") String docDateString) {
        try {
            LocalDate parsedDate = LocalDate.parse(docDateString);
            return new ResponseEntity<>(commentService.getByDocIdAndVersionDate(
                    docId, parsedDate), HttpStatus.OK);
        } catch (DateTimeParseException e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping()
    public ResponseEntity<CommentEntity> add(@RequestBody CommentDto comment) {
        return new ResponseEntity<>(commentService.saveComment(comment), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable("id") String id, @RequestBody CommentEntity comment) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") String id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
